from opcua import Client

url = "opc.tcp://169.254.245.101:4840"  # add your own IP address

Client = Client(url)
# Finds what server that the client is connected to
server_addr = Client.server_url[1]
# Connects to the server
Client.connect()
# Prints out if the server was connected
print("Client connected succesfully to server: %s" % server_addr)
# locate the node and store it as farmbot_ID
farmbot_ID = Client.get_node("ns=2;i=2")

while True:
    user = input("Do you wan to execute a command? - y/n?")
    if user == "y":
        # Enter predefined command that you want to send
        Farmbot_set = farmbot_ID.set_value("G00 X2000 Y800 Z-200\r\n")
    else:
        print("Closing program")
        Client.disconnect()
        break
