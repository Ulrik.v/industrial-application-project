# Learning goals summary


## What is PLC?

PLC is also known as **programmic logic controller**

* It recieves information from connected sensors or input devices, then processes the data and trigger outputs that is based on pre-programmed parameteres
* PLC can monitor and record run-time data such as
- Macine productivity, operating temperature, start/stop, alarms for malfunctions and much more

### what set apart the PLC from microcontroleers or other industriial PCS

* I/O - THE PLC's CPu stores and processes program data, but input and out modules connect the PLC to the rest of the machine
- The I/O modules are what provide information to the CPU and trigger results. The I/O are either analog or digital
- Input devices could be sensors, switches, while output could be relays, lights
* Communication - PLC offer a range of ports and communication protocols to ensure that he PLC can comunicatewith other systems

### How is it programmed?

* With ladder logic or "C"
- Ladder logic read left to right.

### Sources

* https://www.unitronicsplc.com/what-is-plc-programmable-logic-controller/

## Some Industrial communication Protocols

### what is fieldbus?

Means communicating with input devices like sensors, swtiches and output devices, (valves, drives) wihout need to connect to each individual device back to the controller 

## 3 differenct kind of communcation protocols

* PROFINET is a mechanism to exchange data between controllers and devices. Controllers could be PLCs, DCSs, or PACs (Programmable Logic Controllers, Distributed Control Systems, or Programmable Automation Controllers.). Devices could be I/O blocks, vision systems, RFID readers, drives, process instruments, proxies, or even other controllers.
* Profibus: is an internal fieldbus communication standard for linking process control and plant automation modules. Instead of running indivudual cacbles from a main controller to each sensor and actuator, single multi-drop cable is used to connect all devices
* Modbus is a Master/salve communication protocol. The protocol provides for one master device and up to 247 slave devices on a common line. Each device is assigned an address to differentiate them from other conneced devices


### Sources

* https://www.youtube.com/watch?v=Al-deBn0ywU
* https://profinetuniversity.com/profinet-basics/definition-profinet/

## Working princible of a working robot ARM

**Cartesian Robots**

* Also called linear robots or **Gantry robots**
* they work on three linear axes and use the cartesian coordinate system (X, Y and Z)
* They move in straight lines on 3-axis (up and down, in and out and side to sid)
* They are very flexible, can be adujusted like speed, precision, stroke lengh and size by the user
* Used for like 3D printing. **The Farmbot is a Gantry Robot**

**Scara Robots**

* CAlled Selective Compliance Assembly Robot Arm
* Function on 3-axis (X, Y and Z) and have rotary motion aswell
* Commonly faster moving than Cartesian robots
* Used for like bio-med application

**Articulated Robots**

* Mechanical movement and configuration resembles a human arm
* ARm is mounted to a base with a twisting joint
* It can feature anywhere from two to ten rotary joints
* Joints acts as axes, more allow greate degree of motion
* Used for like arc welding, material handling, packaging

**Cylindrical robots**

* has a rotary joint at the base and prismatic joint to connect the links
* Used for like tight workspaces for simple assembly

**Delta Robots**

* Also known as paralle robots
* Posses three arms connected to a single base which is mounted above workspace
* They work in a dome shape and can move both delicately and very precise at high speeds
* Used for like place application in the food or electronic industries


**Collaborative Robots**

* can directly and safely interact with humans in a shared workspace
* used for like pick and place

### Sources

* https://processsolutions.com/what-are-the-different-types-of-industrial-robots-and-their-applications/
* https://www.online-sciences.com/wp-content/uploads/2019/02/Industrial-Robots-2222.jpg

## OPCUA VS MQTT (What are they? and what is different fro eachother)

### OPCUA
* OPCUA stands for OPC Unified Architecture and was published in 2008
* Typical use cases include industrial automation and process control applications as well as client-server interactions between components such as devices or applications.
* OPC UA is based on client/server architecture and uses TCP/IP and HTTP/SOAP as underlying technologies
* OPC UA defines a complete data type system. Resources in OPC UA are referred to as nodes. The different nodes that make up a system are individually addressable and can be structured as data objects from structured data types of varying complexity.
* In OPC UA, the client decides when and which data the server retrieves from the underlying systems. For example, even if the server subscribes to periodic status updates, it is up to the client to determine how often the server polls the devices and systems.


### MQTT

* Is lightweight MQTT protocol is the publish-subscribe pattern
* allows any number of data consumers to subscribe to individual topics or subject areas and receive the messages published about them
* The central component of MQTT is always an MQTT broker. It is the responsibility of the broker to fully implement the MQTT specification

### Why OPCUA instead?

* One main cause is using closed loop process on LAN, where there is a need for machines to talk in real-time, OPC UA is great at that
* OPC UA is designed with industrial application in mind. Most industrial equipment last longer than most IT-devices
* OPC UA functions for a faster response time Than MQTT. This makes it very suitable for high speed motion and sensing application

### sources

* https://www.hivemq.com/iiot-protocols-opc-ua-mqtt-sparkplug-comparison/
* https://www.outlierautomation.com/blog/202020/12/16/whats-the-difference-between-opc-ua-and-mqtt

## G AND F CODES

* G-codes is a programming language for Computer Numerical Control
* The language is used to tell a machine what to do or how to do something
- LIke where to move, how fast to move and what path to follow

An example could be:

G## X## Y## Z## F##

- G: is the G-code command
- X, Y, Z: are the position or coordinates
- F: Value that is set as the **feed rate** or the speed at which the move will be done

### Sources

* https://howtomechatronics.com/tutorials/g-code-explained-list-of-most-important-g-code-commands/

## Farmbot Documentaion (How does the farmbot work right now?)

The summary of the farmbot software:

![Arduino_software](https://software.farm.bot/v14/FarmBot-Software/_images/farmbot_software_high_level_overview.jpg)


### Sources

* https://software.farm.bot/v14/FarmBot-Software/intro
* https://github.com/FarmBot/farmbot-arduino-firmware




