from opcua import Server
from time import sleep
import serial

# If you miss node id you can find it by using: print(Param.get_children())

server = Server()

url = "opc.tcp://169.254.245.101:4840"  # Change to your own Raspberry pi IP address to test
server.set_endpoint(url)

name = "OPCUA_RPI_SERVER"
addspace = server.register_namespace(name)

node = server.get_objects_node()

Param = node.add_object(addspace, "Parameters")

client_command = Param.add_variable(addspace, "clientState", 0)

client_command.set_writable()
server.start()


def command(ser, send_command):
    print(send_command)
    ser.write(str.encode(send_command))
    sleep(1)


def main():
    while True:
        current_value = client_command.get_value()
        print(current_value)
        sleep(2)
        if current_value != 1 and current_value != 0:
            ser = serial.Serial('/dev/ttyACM0', 115200)
            sleep(2)
            command(ser, "F22 P2 V1 Q0\r\n")
            print(current_value)
            command(ser, current_value)
            break
        else:
            pass


main()
