# Industrial application project: Tinkering with the Farmbot

## What does the project cover?

The Farmbot software now operate with a MQTT gateway as the middle man. It's responsible for communication between client
and the Farmdruino. But because of the needs in the industrial automation environment, a possible solution could be switching it out
with something like OPCUA

## The proof of concept

To replace the MQTT gateway with OPCUA using server and client communication


**By Matas, Nikandras and Ulrik**